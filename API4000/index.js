const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const helmet = require('helmet');
const morgan = require('morgan');

// defining the Express app
const app = express();
// defining an array to work as the database (temporary solution)
const ads = [
  {title: 'Hello, world (again)!'}
];

// adding Helmet to enhance your Rest API's security
app.use(helmet());

// using bodyParser to parse JSON bodies into JS objects
app.use(bodyParser.json());

// enabling CORS for all requests
app.use(cors());

// adding morgan to log HTTP requests
app.use(morgan('combined'));

app.get('/info', (req, res) => {
 
    res.send("Jaime Ismael Belloso Garcia - 201325557");
});

app.post('/restar', (req, res) => {
 
  res.send(`resultado: ${parseInt(req.query.num1) - parseInt(req.query.num2)}` );
});

// starting the server
app.listen(4000, () => {
  console.log('listening on port 4000');
});